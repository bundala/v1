import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

//bootstrap5
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap"

//font awesome
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faPencil, faTrash } from "@fortawesome/free-solid-svg-icons";


library.add(faTrash,faPencil);


createApp(App).component("font-awesome-icon", FontAwesomeIcon).use(router).mount('#app')
