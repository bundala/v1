import { createRouter, createWebHashHistory } from 'vue-router'
import BmiView from '../views/BmiView.vue'
import TodoView from '../views/TodoView.vue'
import LoginView from '../views/LoginView.vue'

const routes = [
  {
    path: '/bmi',
    name: 'BMI',
    component: BmiView
  },
  {
    path: '/todoApp',
    name: 'todoApp',
    component:TodoView
  },
  {
    path: '/',
    name: 'login',
    component: LoginView
    
}
]

const router = createRouter({
  history: createWebHashHistory(),
  routes,
  mode: 'abstract',
})

// route guard
// this is a function that will run before going to each route
router.beforeEach(async(to, from) => {

  let isAuthenticated = localStorage.getItem('isAuth');

  // this will redirect immediately if user is not login and try to access the About page
  if ((!isAuthenticated && to.name == 'BMI' || !isAuthenticated && to.name == 'todoApp')) {
      return { name: 'login' }
  }

  // this will redirect if the user is login and try to access login page
  if (isAuthenticated && to.name === 'Login') {
      return { name: 'Home' }
  }
})

export default router
